-- Create a mocked movie for test.
insert into movie (MOVIE_GENRE, MOVIE_TITLE,MOVIE_TIME,MOVIE_LOCATION) values ('Action', '007 - TEST',PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi Mirim');
insert into movie (MOVIE_GENRE, MOVIE_TITLE,MOVIE_TIME,MOVIE_LOCATION) values ('Animation', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi Mirim');
insert into movie (MOVIE_GENRE, MOVIE_TITLE,MOVIE_TIME,MOVIE_LOCATION) values ('Comedy', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi Mirim');
insert into movie (MOVIE_GENRE, MOVIE_TITLE,MOVIE_TIME,MOVIE_LOCATION) values ('Documentary', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi Mirim');
insert into movie (MOVIE_GENRE, MOVIE_TITLE,MOVIE_TIME,MOVIE_LOCATION) values ('Thriller', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi Mirim');