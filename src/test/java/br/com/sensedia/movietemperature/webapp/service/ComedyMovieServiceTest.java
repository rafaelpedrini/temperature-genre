package br.com.sensedia.movietemperature.webapp.service;

import br.com.sensedia.movietemperature.common.configuration.BaseTestRunner;
import br.com.sensedia.movietemperature.common.enumerable.ConfigurationCatalog;
import br.com.sensedia.movietemperature.common.util.JSONUtil;
import br.com.sensedia.movietemperature.webapp.dto.MovieDTO;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for testing all existing {@link MovieService}.
 *
 * @author rafaelcustodio
 */
public class ComedyMovieServiceTest extends BaseTestRunner {



    @Autowired
    private MovieService movieService;

    @Test
    public void testGetRightMovieList() throws IOException {
        final List<MovieDTO> moviesPerTemperatureDesired = this.movieService.findByTemperature(40.0);
        final List<MovieDTO> mockList = new ArrayList<>();
        mockList.add(this.buildComedyMovieMock());
        Assert.assertEquals(moviesPerTemperatureDesired,mockList);
    }


    /**
     * Create a mock of a list of comedy movies used to for the unit tests.
     * @return The mocked list movie request.
     * @throws IOException Error while reading the files from the test resources.
     */
    private MovieDTO buildComedyMovieMock() throws IOException {

        // Get the mocked information for a list movie associated to the request.
        final MovieDTO comedyMovie = (MovieDTO) JSONUtil
                .fileToBean(ConfigurationCatalog.DEFAULT_COMEDY_MOVIE_FILE.getValue(),
                        TypeFactory.defaultInstance().constructType(MovieDTO.class));

        return comedyMovie;
    }


}
