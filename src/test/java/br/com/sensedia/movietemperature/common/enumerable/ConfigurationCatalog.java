package br.com.sensedia.movietemperature.common.enumerable;

/**
 * Catalog with all main information used by the unit tests.
 *
 * @author rafaelcustodio
 */
public enum ConfigurationCatalog {

    COMMON_BASE_DIRECTORY("/common"),

    MOVIE_BASE_DIRECTORY(COMMON_BASE_DIRECTORY.getValue() + "/movie"),

    DEFAULT_ACTION_MOVIE_FILE(MOVIE_BASE_DIRECTORY.getValue() + "/default-actionmovie.json"),

    DEFAULT_COMEDY_MOVIE_FILE(MOVIE_BASE_DIRECTORY.getValue() + "/default-comedymovie.json"),

    DEFAULT_THRILLER_MOVIE_FILE(MOVIE_BASE_DIRECTORY.getValue() + "/default-thrillermovie.json"),

    DEFAULT_DOCUMENTARY_MOVIE_FILE(MOVIE_BASE_DIRECTORY.getValue() + "/default-documentarymovie.json"),

    DEFAULT_ANIMATION_MOVIE_FILE(MOVIE_BASE_DIRECTORY.getValue() + "/default-animationmovie.json");

    private String value;

    ConfigurationCatalog(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
