package br.com.sensedia.movietemperature;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = {"br.com.sensedia.movietemperature.webapp.model"}, basePackageClasses = {Jsr310JpaConverters.class})
@EnableJpaRepositories(basePackages = "br.com.sensedia.movietemperature.webapp.repository")
public class MovieTemperatureApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieTemperatureApplication.class, args);
	}
}
