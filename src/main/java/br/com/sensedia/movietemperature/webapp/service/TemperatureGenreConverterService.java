package br.com.sensedia.movietemperature.webapp.service;

import br.com.sensedia.movietemperature.webapp.Alias.GenreAlias;
import br.com.sensedia.movietemperature.webapp.model.Movie;

/**
 * Represents a service responsible for performing temperature convertion for an {@link Movie}
 *
 * @author rafaelcustodio
 */
public interface TemperatureGenreConverterService {
    /**
     * Performs the convertion to a genre based on the temperature passed.
     * @param temperature the temperature to be used for the convertion.
     * @return The genre resulted from convertion.
     */
    GenreAlias convertTempToGenre(final Double temperature);
}
