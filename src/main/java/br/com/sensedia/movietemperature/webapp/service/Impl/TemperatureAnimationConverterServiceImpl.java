package br.com.sensedia.movietemperature.webapp.service.Impl;

import br.com.sensedia.movietemperature.webapp.Alias.GenreAlias;
import br.com.sensedia.movietemperature.webapp.service.TemperatureGenreConverterService;
import org.springframework.stereotype.Service;

@Service("TemperatureAnimationConverterServiceImpl")
public class TemperatureAnimationConverterServiceImpl implements TemperatureGenreConverterService {

    private static final long NORMAL_TEMPERATURE = 20;

    private static final long HOT_TEMPERATURE = 36;

    @Override
    public GenreAlias convertTempToGenre(final Double temperature) {
        if(temperature < HOT_TEMPERATURE && temperature > NORMAL_TEMPERATURE){
            return GenreAlias.Animation;
        }
        return null;
    }
}
