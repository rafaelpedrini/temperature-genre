package br.com.sensedia.movietemperature.webapp.Alias;

/**
 * Catalog responsible for defining all the possible Genre types.
 *
 * @author rafaelcustodio
 */
public enum GenreAlias {
    Action,
    Comedy,
    Animation,
    Thriller,
    Documentary
}
