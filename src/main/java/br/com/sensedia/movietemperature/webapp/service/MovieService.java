package br.com.sensedia.movietemperature.webapp.service;

import br.com.sensedia.movietemperature.webapp.dto.MovieDTO;
import br.com.sensedia.movietemperature.webapp.model.Movie;

import java.util.List;


/**
 * Service responsible for handling operations associated to a {@link Movie}.
 *
 * @author rafaelcustodio
 */
public interface MovieService {

    /**
     * Find a movie based on the temperature passed.
     * @return The found movies.
     */
    List<MovieDTO> findByTemperature(final Double temperature);

    /**
     * Find all existing movies into the database.
     * @return The found movies.
     */
    List<MovieDTO>  findAll();

    /**
     * Persist/Update a movie into the database.
     * @param movie The Movie to be persisted.
     * @return The persisted movie.
     */
    MovieDTO save(final MovieDTO movie);

    /**
     * Delete a certain movie by their identifier.
     * @param id The identifier of the movie.
     */
    void delete(final Long id);
}
