package br.com.sensedia.movietemperature.webapp.service.Impl;

import br.com.sensedia.movietemperature.webapp.Alias.GenreAlias;
import br.com.sensedia.movietemperature.webapp.service.TemperatureGenreConverterService;
import org.springframework.stereotype.Service;

@Service("TemperatureDocumentaryConverterServiceImpls")
public class TemperatureDocumentaryConverterServiceImpl implements TemperatureGenreConverterService {

    private static final long ZERO_TEMPERATURE = 0;

    @Override
    public GenreAlias convertTempToGenre(final Double temperature) {
        if (temperature < ZERO_TEMPERATURE) {
            return GenreAlias.Documentary;
        }
        return null;
    }
}
