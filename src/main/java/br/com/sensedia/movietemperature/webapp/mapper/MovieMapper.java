package br.com.sensedia.movietemperature.webapp.mapper;

import br.com.sensedia.movietemperature.webapp.dto.MovieDTO;
import br.com.sensedia.movietemperature.webapp.model.Movie;
import org.springframework.util.ObjectUtils;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Class responsible for handling the mapping for a {@link Movie}.
 *
 * @author rafaelcustodio
 */
@Mapper(uses = {Movie.class}, imports = {java.util.Date.class})
public class MovieMapper {

    private MovieMapper() {}

    /**
     * Convert a list of entities into a list of data transfer objects.
     * @param entities The list of entities to be converted.
     * @return The converted list of entities.
     */
    public static List<MovieDTO> entityToDTO(List<Movie> entities) {
        if(!ObjectUtils.isEmpty(entities)) {
            return entities.stream().map(MovieMapper::entityToDTO).collect(Collectors.toList());
        }
        return null;
    }

    /**
     * Convert a list of data transfer object into a list of entities.
     * @param dtos The list of data transfer object to be converted.
     * @return The converted list of data transfer object.
     */
    public static List<Movie> dtoToEntity(List<MovieDTO> dtos) {
        if(!ObjectUtils.isEmpty(dtos)) {
            return dtos.stream().map(MovieMapper::dtoToEntity).collect(Collectors.toList());
        }
        return null;
    }

    /**
     * Convert a entity into a data transfer object.
     * @param entity The entity to be converted.
     * @return The converted entity.
     */
    public static MovieDTO entityToDTO(Movie entity) {
        if(entity != null) {
            MovieDTO dto = new MovieDTO();
            dto.setMovieGenre(entity.getMovieGenre());
            dto.setMovieLocation(entity.getMovieLocation());
            dto.setMovieTime(entity.getMovieTime());
            dto.setMovieTitle(entity.getMovieTitle());
            return dto;
        }
        return null;
    }
    /**
     * Convert a data transfer object into an entity.
     * @param dto The data transfer object to be converted.
     * @return The converted data transfer object.
     */
    public static Movie dtoToEntity(MovieDTO dto) {
        if(dto != null) {
            Movie entity = new Movie();
            entity.setMovieGenre(dto.getMovieGenre());
            entity.setMovieLocation(dto.getMovieLocation());
            entity.setMovieTime(dto.getMovieTime());
            entity.setMovieTitle(dto.getMovieTitle());
            return entity;
        }
        return null;
    }


}
