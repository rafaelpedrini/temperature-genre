package br.com.sensedia.movietemperature.webapp.controller;

import br.com.sensedia.movietemperature.webapp.dto.MovieDTO;
import br.com.sensedia.movietemperature.webapp.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Class responsible for mapping all the endpoints that handle process related to a {@link br.com.sensedia.movietemperature.webapp.model.Movie}.
 *
 * @author rafaelcustodio
 */

@RestController
@RequestMapping(path = "/movies",  produces = MediaType.APPLICATION_JSON_VALUE)
public class MovieController {

    @Autowired
    private MovieService movieService;


    @GetMapping("/{temperature}")
    @ResponseStatus(HttpStatus.OK)
    public List<MovieDTO> findByTemperature(@PathVariable("temperature") final Double temperature) {
        return this.movieService.findByTemperature(temperature);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<MovieDTO> findAll(){
        return this.movieService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MovieDTO save(@RequestBody @Valid final MovieDTO movie) {
        return this.movieService.save(movie);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") final Long id) {
        this.movieService.delete(id);
    }


}
