package br.com.sensedia.movietemperature.webapp.service.Impl;

import br.com.sensedia.movietemperature.webapp.Alias.GenreAlias;
import br.com.sensedia.movietemperature.webapp.service.TemperatureGenreConverterService;
import org.springframework.stereotype.Service;

@Service("TemperatureThrillerConverterServiceImpl")
public class TemperatureThrillerConverterServiceImpl implements TemperatureGenreConverterService {

    private static final long ZERO_TEMPERATURE = 0;

    private static final long NORMAL_TEMPERATURE = 20;

    @Override
    public GenreAlias convertTempToGenre(final Double temperature) {
        if(temperature < NORMAL_TEMPERATURE && temperature > ZERO_TEMPERATURE){
            return GenreAlias.Thriller;
        }
        return null;
    }
}
