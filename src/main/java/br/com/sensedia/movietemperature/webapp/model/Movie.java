package br.com.sensedia.movietemperature.webapp.model;

import br.com.sensedia.movietemperature.webapp.Alias.GenreAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


/**
 * Class used to represent the information of a movie stored on the database.
 *
 * @author rafaelcustodio
 */
@Entity
@Table(name = "MOVIE")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @NotEmpty(message = "Genre name is mandatory.")
    @Column(name = "MOVIE_GENRE")
    private GenreAlias movieGenre;

    @NotEmpty(message = "Movie title is mandatory.")
    @Column(name = "MOVIE_TITLE")
    private String movieTitle;

    @NotNull(message = "Movie time is mandatory.")
    @Column(name = "MOVIE_TIME")
    private LocalDateTime movieTime;

    @NotEmpty(message = "Movie location is mandatory.")
    @Column(name = "MOVIE_LOCATION")
    private String movieLocation;

    public GenreAlias getMovieGenre() {
        return movieGenre;
    }

    public void setMovieGenre(GenreAlias movieGenre) {
        this.movieGenre = movieGenre;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public LocalDateTime getMovieTime() {
        return movieTime;
    }

    public void setMovieTime(LocalDateTime movieTime) {
        this.movieTime = movieTime;
    }

    public String getMovieLocation() {
        return movieLocation;
    }

    public void setMovieLocation(String movieLocation) {
        this.movieLocation = movieLocation;
    }
}
