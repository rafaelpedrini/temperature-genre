package br.com.sensedia.movietemperature.webapp.service.Impl;

import br.com.sensedia.movietemperature.webapp.Alias.GenreAlias;
import br.com.sensedia.movietemperature.webapp.service.TemperatureGenreConverterService;
import org.springframework.stereotype.Service;

@Service("TemperatureActionConverterServiceImpl")
public class TemperatureActionConverterServiceImpl implements TemperatureGenreConverterService {

    private static final long SENEGAL_TEMPERATURE = 40;


    @Override
    public GenreAlias convertTempToGenre(final Double temperature) {
        if(temperature > SENEGAL_TEMPERATURE ){
            return GenreAlias.Action;
        }
        return null;
    }
}
