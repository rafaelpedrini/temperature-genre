package br.com.sensedia.movietemperature.webapp.service.Impl;

import br.com.sensedia.movietemperature.webapp.Alias.GenreAlias;
import br.com.sensedia.movietemperature.webapp.dto.MovieDTO;
import br.com.sensedia.movietemperature.webapp.mapper.MovieMapper;
import br.com.sensedia.movietemperature.webapp.model.Movie;
import br.com.sensedia.movietemperature.webapp.repository.MovieRepository;
import br.com.sensedia.movietemperature.webapp.service.MovieService;
import br.com.sensedia.movietemperature.webapp.service.TemperatureGenreConverterService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.util.List;

/**
 * Service responsible for handling operations associated to a {@link Movie}.
 *
 * @author rafaelcustodio
 */


@Service
@Transactional
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TemperatureGenreConverterService temperatureGenreConverter;


    public List<MovieDTO> findByTemperature(final Double temperature) {
        final GenreAlias genreAlias =  this.temperatureGenreConverter.convertTempToGenre(temperature);
        return MovieMapper.entityToDTO(this.movieRepository.findAllByGenre(genreAlias));
    }

    public List<MovieDTO> findAll(){
        return MovieMapper.entityToDTO(this.movieRepository.findAll());
    }

    @Override
    public MovieDTO save(final MovieDTO movie) {
        final Movie movieToPersist = MovieMapper.dtoToEntity(movie);
        return MovieMapper.entityToDTO(this.movieRepository.save(movieToPersist));
    }

    @Override
    public void delete(final Long id) {
        this.movieRepository.delete(id);
    }
}
