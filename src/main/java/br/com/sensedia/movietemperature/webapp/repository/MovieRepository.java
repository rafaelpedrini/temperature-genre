package br.com.sensedia.movietemperature.webapp.repository;

import br.com.sensedia.movietemperature.webapp.Alias.GenreAlias;
import br.com.sensedia.movietemperature.webapp.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * The Data Access Object used to retrieve data of a {@link Movie}.
 *
 * @author rafaelcustodio
 */
public interface MovieRepository extends JpaRepository<Movie, Long> {

    List<Movie> findAllByGenre(@Param("genreAlias") final GenreAlias genreAlias);

}
