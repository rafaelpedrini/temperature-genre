package br.com.sensedia.movietemperature.webapp.dto;

import br.com.sensedia.movietemperature.webapp.Alias.GenreAlias;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Class used to represent the information of a movie stored on the database.
 *
 * @author rafaelcustodio
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MovieDTO {


    private Long id;

    @NotEmpty(message = "Genre name is mandatory.")
    @Column(name = "movieGenre")
    private GenreAlias movieGenre;

    @NotEmpty(message = "Movie title is mandatory.")
    @Column(name = "movieGenre")
    private String movieTitle;

    @NotNull(message = "Movie time is mandatory.")
    @Column(name = "movieGenre")
    private LocalDateTime movieTime;

    @NotEmpty(message = "Movie location is mandatory.")
    @Column(name = "movieGenre")
    private String movieLocation;

    public GenreAlias getMovieGenre() {
        return movieGenre;
    }

    public void setMovieGenre(GenreAlias movieGenre) {
        this.movieGenre = movieGenre;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public LocalDateTime getMovieTime() {
        return movieTime;
    }

    public void setMovieTime(LocalDateTime movieTime) {
        this.movieTime = movieTime;
    }

    public String getMovieLocation() {
        return movieLocation;
    }

    public void setMovieLocation(String movieLocation) {
        this.movieLocation = movieLocation;
    }
}
