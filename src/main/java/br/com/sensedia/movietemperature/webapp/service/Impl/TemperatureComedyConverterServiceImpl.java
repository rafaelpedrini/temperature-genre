package br.com.sensedia.movietemperature.webapp.service.Impl;

import br.com.sensedia.movietemperature.webapp.Alias.GenreAlias;
import br.com.sensedia.movietemperature.webapp.service.TemperatureGenreConverterService;
import org.springframework.stereotype.Service;

@Service("TemperatureComedyConverterServiceImpl")
public class TemperatureComedyConverterServiceImpl implements TemperatureGenreConverterService {

    private static final long SENEGAL_TEMPERATURE = 40;

    private static final long HOT_TEMPERATURE = 36;

    @Override
    public GenreAlias convertTempToGenre(final Double temperature) {
        if(temperature < SENEGAL_TEMPERATURE && temperature > HOT_TEMPERATURE){
            return GenreAlias.Comedy;
        }
        return null;
    }
}
