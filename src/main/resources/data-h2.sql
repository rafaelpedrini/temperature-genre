-- Create the catalog of possible action movies.
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Action', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi-mirim');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Action', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi-guacu');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Action', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Campinas');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Action', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'São Carlos');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Action', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'São Paulo');

-- Create the catalog of possible Comedy movies.
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Comedy', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi-mirim');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Comedy', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi-guacu');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Comedy', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Campinas');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Comedy', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'São Carlos');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Comedy', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'São Paulo');

-- Create the catalog of possible Animation movies.
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Animation', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi-mirim');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Animation', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi-guacu');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Animation', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Campinas');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Animation', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'São Carlos');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Animation', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'São Paulo');

-- Create the catalog of possible Thriller movies.
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Thriller', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi-mirim');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Thriller', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi-guacu');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Thriller', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Campinas');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Thriller', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'São Carlos');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Thriller', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'São Paulo');

-- Create the catalog of possible Documentary movies.
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Documentary', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi-mirim');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Documentary', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Mogi-guacu');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Documentary', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'Campinas');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Documentary', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'São Carlos');
insert into movie (MOVIE_GENRE, MOVIE_TITLE, MOVIE_TIME, MOVIE_LOCATION) values ('Documentary', '007 - TEST', PARSEDATETIME('2016-11-09 00:00', 'yyyy-MM-dd HH:mm'), 'São Paulo');
